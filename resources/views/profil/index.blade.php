@extends('layouts.app')

@section('content')
<div class="container-fluid px-2 px-md-3">
    <div class="page-header min-height-300 border-radius-xl mt-4"
        style="background-image: url('https://images.unsplash.com/photo-1531512073830-ba890ca4eba2?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&amp;ixlib=rb-1.2.1&amp;auto=format&amp;fit=crop&amp;w=1920&amp;q=80');">
        <span class="mask bg-gradient-info opacity-6"></span>
    </div>
    <div class="card card-body mx-3 mx-md-4 mt-n6">
        <div class="row gx-4 mb-2">
            <div class="col-auto">
                <div class="avatar avatar-xl position-relative">
                    <img src="../assets/img/bruce-mars.jpg" alt="profile_image"
                        class="w-100 border-radius-lg shadow-sm">
                </div>
            </div>
            <div class="col-auto my-auto">
                <div class="h-100">
                    <h5 class="mb-1 text-uppercase">
                        {{ Auth()->user()->name }}
                    </h5>
                    <span class="badge bg-gradient-success">{{ Auth()->user()->role }}</span>
                </div>
            </div>
        </div>
        <div class="row mb-2">
            <div class="col-12">
                <div class="card card-plain h-100">
                    <div class="card-header pb-0 p-3">
                        <div class="row">
                            <div class="col-md-8 d-flex align-items-center">
                                <h6 class="mb-0">Tentang Diri</h6>
                            </div>
                        </div>
                    </div>
                    <div class="card-body p-3">
                        <p class="text-sm text-justify">
                            {{ Auth()->user()->tentang_diri }}
                        </p>
                        <ul class="list-group">
                            <li class="list-group-item border-0 ps-0 text-sm"><strong class="text-dark">Tanggal Lahir
                                    :</strong>&nbsp;{{ Auth()->user()->tanggal_lahir }}</li>
                            <li class="list-group-item border-0 ps-0 text-sm"><strong class="text-dark">Alamat
                                    :</strong>&nbsp;{{ Auth()->user()->alamat }}</li>
                        </ul>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="input-group input-group-outline my-3">
                                    <label class="form-label"><strong class="text-dark">Email :</strong>&nbsp;{{
                                        Auth()->user()->email }}</label>
                                    <input type="email" class="form-control" disabled>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="input-group input-group-outline my-3">
                                    <label class="form-label"><strong class="text-dark">Nomor Telp :</strong>&nbsp; (62)
                                        888 - 8888 - 8888</label>
                                    <input type="email" class="form-control" disabled>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div>
            <a href="#" type="button" class="btn bg-gradient-info mb-0">Ubah Profil</a>
        </div>
    </div>
</div>
@endsection