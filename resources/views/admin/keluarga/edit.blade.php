@extends('layouts.app')

@section('content')
<div class="container-fluid py-4">
    <div class="row">
        <div class="col-12">
            <div class="card my-4">
                <div class="card-header p-0 position-relative mt-n4 mx-3 z-index-2">
                    <div class="bg-gradient-info shadow-info border-radius-lg pt-4 pb-3">
                        <h6 class="text-white text-capitalize ps-3">Tambah Data Keluarga</h6>
                    </div>
                </div>
                <div class="card-body px-0 pb-2">
                    <div class="mx-4">
                        <form action="{{ route('admin-keluarga.update', $keluargaAdmin->id) }}" method="POST">
                            @csrf
                            @method('PUT')
                            <p class="text-xs text-uppercase font-weight-bolder">Nama</p>
                            <div class="input-group input-group-outline my-3">
                                <input type="text" name="nama" class="form-control">
                            </div>
                            @error('nama')
                            <p class='text-danger mb-0 text-xs pt-1'> {{ $message }} </p>
                            @enderror
                            <p class="text-xs text-uppercase font-weight-bolder">Jenis Kelamin</p>
                            <div class="input-group input-group-outline my-3">
                                <input type="text" name="jenis_kelamin" class="form-control">
                            </div>
                            @error('jenis_kelamin')
                            <p class='text-danger mb-0 text-xs pt-1'> {{ $message }} </p>
                            @enderror
                            <p class="text-xs text-uppercase font-weight-bolder">Status</p>
                            <div class="input-group input-group-outline my-3">
                                <input type="text" name="status" class="form-control">
                            </div>
                            @error('status')
                            <p class='text-danger mb-0 text-xs pt-1'> {{ $message }} </p>
                            @enderror
                            <p class="text-xs text-uppercase font-weight-bolder">Tanggal Lahir</p>
                            <div class="input-group input-group-outline my-3">
                                <input type="date" name="tanggal_lahir" class="form-control">
                            </div>
                            @error('tanggal_lahir')
                            <p class='text-danger mb-0 text-xs pt-1'> {{ $message }} </p>
                            @enderror
                            <button type="submit" class="btn bg-gradient-info mt-2">Tambah</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection