@extends('layouts.app')

@section('content')
<div class="container-fluid py-4">
    <div class="row">
        <div class="col-12">
            <div class="card my-4">
                <div class="card-header p-0 position-relative mt-n4 mx-3 z-index-2">
                    <div class="bg-gradient-info shadow-info border-radius-lg pt-4 pb-3">
                        <h6 class="text-white text-capitalize ps-3">Tambah Data Pelatihan</h6>
                    </div>
                </div>
                <div class="card-body px-0 pb-2">
                    <div class="mx-4">
                        <form action="{{ route('admin-pelatihan.store') }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <p class="text-xs text-uppercase font-weight-bolder">Sertifikat</p>
                            <div class="input-group input-group-outline my-3">
                                <input accept="image/*" type="file" name="sertifikat_pelatihan" class="form-control"
                                onchange="previewImage()">
                            </div>
                            @error('sertifikat_pelatihan')
                            <p class='text-danger mb-0 text-xs pt-1'> {{ $message }} </p>
                            @enderror
                            <p class="text-xs text-uppercase font-weight-bolder">Nama Pelatihan</p>
                            <div class="input-group input-group-outline my-3">
                                <input type="text" name="nama_pelatihan" class="form-control">
                            </div>
                            @error('nama_pelatihan')
                            <p class='text-danger mb-0 text-xs pt-1'> {{ $message }} </p>
                            @enderror
                            <p class="text-xs text-uppercase font-weight-bolder">Tanggal Pelatihan</p>
                            <div class="input-group input-group-outline my-3">
                                <input type="date" name="tanggal_pelatihan" class="form-control">
                            </div>
                            @error('tanggal_pelatihan')
                            <p class='text-danger mb-0 text-xs pt-1'> {{ $message }} </p>
                            @enderror
                            <p class="text-xs text-uppercase font-weight-bolder">Deskripsi</p>
                            <div class="input-group input-group-outline my-3">
                                <textarea name="deskripsi" class="form-control" cols="30" rows="10"></textarea>
                            </div>
                            @error('deskripsi')
                            <p class='text-danger mb-0 text-xs pt-1'> {{ $message }} </p>
                            @enderror
                            <button type="submit" class="btn bg-gradient-info mt-2">Tambah</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card my-4">
                <div class="card-header p-0 position-relative mt-n4 mx-3 z-index-2">
                    <div class="bg-gradient-info shadow-info border-radius-lg pt-4 pb-3">
                        <h6 class="text-white text-capitalize ps-3">Data pelatihan</h6>
                    </div>
                </div>
                <div class="card-body px-0 pb-2">
                    <div class="table-responsive p-0">
                        <table class="table align-items-center mb-0">
                            <thead>
                                <tr>
                                    <th class="text-uppercase text-xs font-weight-bolder">
                                        ID</th>
                                    <th class="text-uppercase text-xs font-weight-bolder">
                                        ID Karyawan</th>
                                    <th class="text-uppercase text-xs font-weight-bolder">
                                        Sertifikat</th>
                                    <th class="text-uppercase text-xs font-weight-bolder">
                                        Nama Pelatihan</th>
                                    <th class="text-uppercase text-xs font-weight-bolder">
                                        Tanggal Pelatihan</th>
                                    <th class=""></th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                $i = 1;
                                @endphp
                                @foreach ($pelatihanAdmin as $item)
                                <tr>
                                    <td>
                                        <div class="px-3">{{ $i }}.</div>
                                    </td>
                                    <td>
                                        <div class="px-3">{{ $item->user_id }}</div>
                                    </td>
                                    <td>
                                        <div class="px-3">IMG</div>
                                    </td>
                                    <td>
                                        <div class="px-3">{{ $item->nama_pelatihan }}</div>
                                    </td>
                                    <td>
                                        <div class="px-3">{{ $item->tanggal_pelatihan}}</div>
                                    </td>
                                    <td>
                                        <div class="px-3">{!! Str::words($item->deskripsi, 20) !!}</div>
                                    </td>
                                    <td>
                                        <div class="px-3">
                                            <a href="{{ route('admin-pelatihan.update', $item->id) }}"
                                                class="btn btn-success btn-sm mb-1">
                                                <svg xmlns="http://www.w3.org/2000/svg"
                                                    class="icon icon-tabler icon-tabler-edit" width="15" height="15"
                                                    viewBox="0 0 24 24" stroke-width="2" stroke="currentColor"
                                                    fill="none" stroke-linecap="round" stroke-linejoin="round">
                                                    <path stroke="none" d="M0 0h24v24H0z" fill="none" />
                                                    <path
                                                        d="M7 7h-1a2 2 0 0 0 -2 2v9a2 2 0 0 0 2 2h9a2 2 0 0 0 2 -2v-1" />
                                                    <path
                                                        d="M20.385 6.585a2.1 2.1 0 0 0 -2.97 -2.97l-8.415 8.385v3h3l8.385 -8.415z" />
                                                    <path d="M16 5l3 3" />
                                                </svg>
                                            </a>
                                            <form onsubmit="return confirm('Sure To Delete This Data')"
                                                action="{{ route('admin-pelatihan.destroy', $item->id) }}" method="POST">
                                                @csrf
                                                @method('DELETE')
                                                <button class="btn btn-sm btn-danger mb-0">
                                                    <svg xmlns="http://www.w3.org/2000/svg"
                                                        class="icon icon-tabler icon-tabler-trash" width="15"
                                                        height="15" viewBox="0 0 24 24" stroke-width="2"
                                                        stroke="currentColor" fill="none" stroke-linecap="round"
                                                        stroke-linejoin="round">
                                                        <path stroke="none" d="M0 0h24v24H0z" fill="none" />
                                                        <path d="M4 7l16 0" />
                                                        <path d="M10 11l0 6" />
                                                        <path d="M14 11l0 6" />
                                                        <path d="M5 7l1 12a2 2 0 0 0 2 2h8a2 2 0 0 0 2 -2l1 -12" />
                                                        <path d="M9 7v-3a1 1 0 0 1 1 -1h4a1 1 0 0 1 1 1v3" />
                                                    </svg>
                                                </button>
                                            </form>
                                        </div>
                                    </td>
                                </tr>
                                @php
                                $i++;
                                @endphp
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection