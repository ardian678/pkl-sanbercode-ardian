<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\DataKeluargeKaryawanController;
use App\Http\Controllers\DataPekerjaanKaryawanController;
use App\Http\Controllers\DataPelatihanKaryawanController;
use App\Http\Controllers\DataPendidikanKaryawanController;
use App\Http\Controllers\DataKeluargaAdminController;
use App\Http\Controllers\DataPekerjaanAdminController;
use App\Http\Controllers\DataPelatihanAdminController;
use App\Http\Controllers\DataPendidikanAdminController;
use App\Http\Controllers\PagesController;
use App\Http\Controllers\UserController;

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::middleware(['auth'])->group(function () {
    // Admin
    Route::resource('admin-keluarga', DataKeluargaAdminController::class);
    Route::resource('admin-pekerjaan', DataPekerjaanAdminController::class);
    Route::resource('admin-pendidikan', DataPendidikanAdminController::class);
    Route::resource('admin-pelatihan', DataPelatihanAdminController::class);
    // Karyawan
    Route::resource('karyawan-keluarga', DataKeluargeKaryawanController::class);
    Route::resource('karyawan-pekerjaan', DataPekerjaanKaryawanController::class);
    Route::resource('karyawan-pendidikan', DataPendidikanKaryawanController::class);
    Route::resource('karyawan-pelatihan', DataPelatihanKaryawanController::class);
});
Route::controller(PagesController::class)->group(function () {
    Route::get('/', 'index');
    Route::get('/{id}', 'single')->name('single');
});
