<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DataPekerjaanAdminController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $pekerjaanAdmin = DB::table('data_pekerjaan_admin')->where('user_id', auth()->user()->id)->get();
        return view('admin.pekerjaan.index', compact('pekerjaanAdmin'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('admin.pekerjaan.index');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        DB::table('data_pekerjaan_admin')->insert([
            'user_id'         => auth()->user()->id,
            'nama_perusahaan' => $request->nama_perusahaan,
            'jabatan'         => $request->jabatan,
            'tahun_mulai'     => $request->tahun_mulai,
            'tahun_akhir'     => $request->tahuan_akhir,
            'created_at'      => now(),
            'updated_at'      => now(),
        ]);

        return redirect()->to('/admin-pekerjaan')->with('success', 'Data Pekerjaan Berhasil Ditambahkan');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $pekerjaanAdmin = DB::table('data_pekerjaan_admin')->get();
        return view('admin.pekerjaan.edit', compact('pekerjaanAdmin'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        DB::table('data_pekerjaan_admin')->insert([
            'user_id'         => auth()->user()->id,
            'nama_perusahaan' => $request->nama_perusahaan,
            'jabatan'         => $request->jabatan,
            'tahun_mulai'     => $request->tahun_mulai,
            'tahun_akhir'     => $request->tahuan_akhir,
            'updated_at'      => now(),
        ]);

        return redirect()->to('/admin-pekerjaan')->with('success', 'Data Pekerjaan Berhasil Diperbarui');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        DB::table('data_pekerjaan_admin')->where('id', $id)->delete();
        return redirect()->to('/admin-pekerjaan')->with('success', 'Data Pekerjaan Berhasil Dihapus');
    }
}
