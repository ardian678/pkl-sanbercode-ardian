<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DataKeluargeKaryawanController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $keluargaKaryawan = DB::table('data_keluarga_karyawan')->where('user_id', auth()->user()->id)->get();
        return view('karyawan.keluarga.index', compact('keluargaKaryawan'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('karyawan.keluarga.index');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        DB::table('data_keluarga_karyawan')->insert([
            'user_id'       => auth()->user()->id,
            'nama'          => $request->nama,
            'jenis_kelamin' => $request->jenis_kelamin,
            'status'        => $request->status,
            'tanggal_lahir' => $request->tanggal_lahir,
            'created_at'    => now(),
            'updated_at'    => now(),
        ]);

        return redirect()->to('/karyawan-keluarga')->with('success', 'Data Keluarga Berhasil Ditambahkan');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $keluargaKaryawan = DB::table('data_keluarga_karyawan')->where('id', $id)->first();
        return view('karyawan.keluarga.edit', compact('keluargaKaryawan'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        DB::table('data_keluarga_karyawan')->insert([
            'user_id'       => auth()->user()->id,
            'nama'          => $request->nama,
            'jenis_kelamin' => $request->jenis_kelamin,
            'status'        => $request->status,
            'tanggal_lahir' => $request->tanggal_lahir,
            'updated_at'    => now(),
        ]);

        return redirect()->to('/karyawan-keluarga')->with('success', 'Data Keluarga Berhasil Diperbarui');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        DB::table('data_keluarga_karyawan')->where('id', $id)->delete();
        return redirect()->to('/karyawan-keluarga')->with('success', 'Data Keluarga Berhasil Dihapus');
    }
}
