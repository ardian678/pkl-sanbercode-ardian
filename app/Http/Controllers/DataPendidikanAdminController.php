<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DataPendidikanAdminController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $pendidikanAdmin = DB::table('data_pendidikan_admin')->where('user_id', auth()->user()->id)->get();
        return view('admin.pendidikan.index', compact('pendidikanAdmin'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('admin.pendidikan.index');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        DB::table('data_pendidikan_admin')->insert([
            'user_id'      => auth()->user()->id,
            'nama_sekolah' => $request->nama_perusahaan,
            'jurusan'      => $request->jurusan,
            'tahun_mulai'  => $request->tahun_mulai,
            'tahun_akhir'  => $request->tahuan_akhir,
            'created_at'   => now(),
            'updated_at'   => now(),
        ]);

        return redirect()->to('/admin-pendidikan')->with('success', 'Data Pendidikan Berhasil Ditambahkan');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $pendidikanAdmin = DB::table('data_pendidikan_admin')->get();
        return view('admin.pendidikan.edit', compact('pendidikanAdmin'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        DB::table('data_pendidikan_admin')->insert([
            'user_id'      => auth()->user()->id,
            'nama_sekolah' => $request->nama_perusahaan,
            'jurusan'      => $request->jurusan,
            'tahun_mulai'  => $request->tahun_mulai,
            'tahun_akhir'  => $request->tahuan_akhir,
            'updated_at'   => now(),
        ]);

        return redirect()->to('/admin-pendidikan')->with('success', 'Data Pendidikan Berhasil Diperbarui');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        DB::table('data_pendidikan_admin')->where('id', $id)->delete();
        return redirect()->to('/admin-pendidikan')->with('success', 'Data pendidikan Berhasil Dihapus');
    }
}
