<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $userProfil = DB::table('users')->get();
        return view('profil.index', compact('userProfil'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $userProfil = DB::table('users')->where('id', $id)->first();
        return view('profil.edit', compact('userProfil'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $attributes = $request->validate([
            'name' => ['required', 'max:255', 'min:2'],
            'email' => ['required', 'email', 'max:255',  Rule::unique('users')->ignore(auth()->user()->id),],
        ]);

        auth()->user()->update([
            'name'          => $request->get('name'),
            'email'         => $request->get('email'),
            'tanggal_lahir' => $request->get('tanggal_lahir'),
            'tentang_diri'  => $request->get('tentang_diri'),
            'alamat'        => $request->get('alamat'),
        ]);
        
        return back()->with('succes', 'Profile succesfully updated');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
