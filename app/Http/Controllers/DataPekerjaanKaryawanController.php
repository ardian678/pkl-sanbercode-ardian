<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DataPekerjaanKaryawanController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $pekerjaanKaryawan = DB::table('data_pekerjaan_karyawan')->where('user_id', auth()->user()->id)->get();
        return view('karyawan.pekerjaan.index', compact('pekerjaanKaryawan'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('karyawan.pekerjaan.index');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        DB::table('data_pekerjaan_karyawan')->insert([
            'user_id'         => auth()->user()->id,
            'nama_perusahaan' => $request->nama_perusahaan,
            'jabatan'         => $request->jabatan,
            'tahun_mulai'     => $request->tahun_mulai,
            'tahun_akhir'     => $request->tahuan_akhir,
            'created_at'      => now(),
            'updated_at'      => now(),
        ]);

        return redirect()->to('/karyawan-pekerjaan')->with('success', 'Data Pekerjaan Berhasil Ditambahkan');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $pekerjaanKaryawan = DB::table('data_pekerjaan_karyawan')->get();
        return view('karyawan.pekerjaan.edit', compact('pekerjaanKaryawan'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        DB::table('data_pekerjaan_karyawan')->insert([
            'user_id'         => auth()->user()->id,
            'nama_perusahaan' => $request->nama_perusahaan,
            'jabatan'         => $request->jabatan,
            'tahun_mulai'     => $request->tahun_mulai,
            'tahun_akhir'     => $request->tahuan_akhir,
            'updated_at'      => now(),
        ]);

        return redirect()->to('/karyawan-pekerjaan')->with('success', 'Data Pekerjaan Berhasil Diperbarui');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        DB::table('data_pekerjaan_karyawan')->where('id', $id)->delete();
        return redirect()->to('/karyawan-pekerjaan')->with('success', 'Data Pekerjaan Berhasil Dihapus');
    }
}
