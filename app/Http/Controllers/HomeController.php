<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $keluargaAdmin = DB::table('data_keluarga_admin')->count();
        $pekerjaanAdmin = DB::table('data_pekerjaan_admin')->count();
        $pelatihanAdmin = DB::table('data_pelatihan_admin')->count();
        $pendidikanAdmin = DB::table('data_pendidikan_admin')->count();
        $keluargaKaryawan = DB::table('data_keluarga_karyawan')->count();
        $pekerjaanKaryawan = DB::table('data_pekerjaan_karyawan')->count();
        $pelatihanKaryawan = DB::table('data_pelatihan_karyawan')->count();
        $pendidikanKaryawan = DB::table('data_pendidikan_karyawan')->count();
        return view('home', compact('keluargaAdmin', 'pekerjaanAdmin', 'pelatihanAdmin', 'pendidikanAdmin', 'keluargaKaryawan', 'pekerjaanKaryawan', 'pelatihanKaryawan', 'pendidikanKaryawan'));
    }
}
