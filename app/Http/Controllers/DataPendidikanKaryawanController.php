<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DataPendidikanKaryawanController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $pendidikanKaryawan = DB::table('data_pendidikan_karyawan')->where('user_id', auth()->user()->id)->get();
        return view('karyawan.pendidikan.index', compact('pendidikanKaryawan'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('karyawan.pendidikan.index');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        DB::table('data_pendidikan_karyawan')->insert([
            'user_id'      => auth()->user()->id,
            'nama_sekolah' => $request->nama_perusahaan,
            'jurusan'      => $request->jurusan,
            'tahun_mulai'  => $request->tahun_mulai,
            'tahun_akhir'  => $request->tahuan_akhir,
            'created_at'   => now(),
            'updated_at'   => now(),
        ]);

        return redirect()->to('/karyawan-pendidikan')->with('success', 'Data Pendidikan Berhasil Ditambahkan');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $pendidikanKaryawan = DB::table('data_pendidikan_karyawan')->get();
        return view('karyawan.pendidikan.edit', compact('pendidikanKaryawan'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        DB::table('data_pendidikan_karyawan')->insert([
            'user_id'      => auth()->user()->id,
            'nama_sekolah' => $request->nama_perusahaan,
            'jurusan'      => $request->jurusan,
            'tahun_mulai'  => $request->tahun_mulai,
            'tahun_akhir'  => $request->tahuan_akhir,
            'updated_at'   => now(),
        ]);

        return redirect()->to('/karyawan-pendidikan')->with('success', 'Data Pendidikan Berhasil Diperbarui');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        DB::table('data_pendidikan_karyawan')->where('id', $id)->delete();
        return redirect()->to('/karyawan-pendidikan')->with('success', 'Data pendidikan Berhasil Dihapus');
    }
}
