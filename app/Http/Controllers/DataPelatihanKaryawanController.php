<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DataPelatihanKaryawanController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $pelatihanKaryawan = DB::table('data_pelatihan_karyawan')
            ->join('users', 'data_pelatihan_karyawan.user_id', '=', 'users.id')
            ->select('data_pelatihan_karyawan.*', 'users.name as nama_karyawan')
            ->where('user_id', auth()->user()->id)
            ->get();
        return view('karyawan.pelatihan.index', compact('pelatihanKaryawan'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('karyawan.pelatihan.index');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'sertifikat_pelatihan' => 'required|mimes:jpg,png,jpeg,pdf',
        ]);

        $sertifikatPelatihan = $request->file('sertifikat_pelatihan')->store('sertifikat');
        $data['sertifikat_pelatihan'] = $sertifikatPelatihan;

        $data = [
            'user_id'              => auth()->id(),
            'sertifikat_pelatihan' => $sertifikatPelatihan,
            'nama_pelatihan'       => request('nama_pelatihan'),
            'tanggal_pelatihan'    => request('tanggal_pelatihan'),
            'deskripsi'            => request('deskripsi'),
        ];

        DB::table('data_pelatihan_karyawan')->insert($data);

        return redirect()->to('/karyawan-pelatihan')->with('success', 'Data Pelatihan Berhasil Ditambahkan');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $pelatihanKaryawan = DB::table('data_pelatihan_karyawan')->get();
        return view('karyawan.pelatihan.edit', compact('pelatihanKaryawan'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $request->validate([
            'sertifikat_pelatihan' => 'required|mimes:jpg,png,jpeg,pdf',
        ]);

        $data = [
            'nama_pelatihan'       => request('nama_pelatihan'),
            'tanggal_pelatihan'   => request('tanggal_pelatihan'),
            'deskripsi'            => request('deskripsi'),
        ];

        if ($request->hasFile('sertifikat')) {
            $sertifikatPelatihan = $request->file('sertifikat')->store('sertifikat');
            $data['sertifikat_path'] = $sertifikatPelatihan;
        }

        DB::table('data_pelatihan_karyawan')->where('id', $id)->update($data);

        return redirect()->to('/karyawan-pelatihan')->with('success', 'Data Pelatihan Berhasil Diperbarui');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        DB::table('data_pelatihan_karyawan')->where('id', $id)->delete();
        return redirect()->to('/karyawan-pelatihan')->with('success', 'Data Pelatihan Berhasil Dihapus');
    }
}
